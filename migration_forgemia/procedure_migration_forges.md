#Procédure de migration des projets de la forge mulcyber (fermeture du service prévue fin 2018)
E.Casellas pour l'équipe Record - mardi, 24. avril 2018

##Introduction

L'environnement collaboratif de développement [Mulcyber](https://mulcyber.toulouse.inra.fr/) est en phase de fermeture. (fermeture definitive du service prévue fin 2018). 
La création de nouveaux utilisateurs et de nouveaux projets est donc désactivée. 
Cette fermeture est concomitante à la publication de la préconisation pour l'utilisation d'une forge logicielle de la part du DTN [le détail ici](https://www6.inra.fr/datapartage/Technologies/Outils/Preconisation-INRA-pour-l-utilisation-d-une-forge-logicielle).

Ce document vise à présenter la procédure d'identification et de migration des projets qui impliquent l'équipe Record.
On va se limiter, au moins dans un premier temps, aux seuls dépôts de sources des projets et laisser de coté les autres services (wiki, forum, documents, ...).


##Identification et caracterisation des dépôts concernés

L'identification des projets concernés par cette procédure à été faite via 2 sources : 

* la liste des projets auquel je (E.Casellas) suis associés (via la page dédié de [mulcyber](https://mulcyber.toulouse.inra.fr/my/)) 
* la liste des projets avec les étiquettes VLE et/ou RECORD  (via la page dédié de [mulcyber](https://mulcyber.toulouse.inra.fr/softwaremap/tag_cloud.php)) 

Pour chacun des dépôts ainsi identifiés les informations suivantes ont été extraites :  

* le nom complet du projet (extrait de la [liste des projets](https://mulcyber.toulouse.inra.fr/my/))
* le nom court du projet (extrait de l'url du projet)
* l'url du projet mulcyber associé (extrait de la [liste des projets](https://mulcyber.toulouse.inra.fr/my/))
* la description courte du projet (extrait de la page de chaque projet)
* le type de gestionnaire de version du dépôt de sources (extrait de la page du dépôt de chaque projet)
* la visibilité du dépôt de sources (extrait de la page du dépôt de chaque projet)
* la version la plus récente de vle compatible (extrait à partir des log du dépôt ou de l'inspection des cmakelists.txt des paquets VLE)
* le mail d'un responsable scientifique du projet (extrait des fichiers Description.txt des paquets VLE ou par expertise)
* le mail d'un mainteneur informatique du projet (extrait des fichiers Description.txt des paquets VLE ou par expertise)
* la date de la dernière modification sur le dépôt de sources  (git log -1 --format=%ci --all) (svn log -r COMMITTED | sed -nE 's/^r.*\| ([0-9]{4}-[0-9]{2}-[0-9]{2} \S+ \S+).*/\1/p' )

Toutes ces informations sont stockées dans un fichier du [sharepoint](https://sites.inra.fr/site/record/_layouts/15/WopiFrame.aspx?sourcedoc=/site/record/Documents%20Ressources%20Informatiques/Migration/RepoList.xlsx&action=default)



##Etapes de la procédure de migration

Pour chacun des projets identifiés précédement on se propose d'appliquer la procédure suivante :

1. Envoi d'un mail aux contacts identifiés. 
    exemple de mail pour le projet XX :
> Bonjour,
    Sauf erreure de notre part, vous avez été identifié comme un responsable du projet XX de la forge mulcyber. Si ce n'est pas le cas, nous vous invitons si possible à nous communiquer les coordonnées d'un interlocuteur. La fermeture de ce service est prévue pour la fin de l'année 2018 (et dès à présent la création de nouveaux utilisateurs et projets sont désactivées).
    Cette fermeture est concomitante à la publication de la préconisation pour l'utilisation d'une forge logicielle de la part du DTN [le détail ici](https://www6.inra.fr/datapartage/Zoom-sur/Preconisation-INRA-pour-l-utilisation-d-une-forge-logicielle).
    Nous (l'équipe Record) vous proposons de prendre en charge la migration du dépôt de sources de ce projet sur le site [forgemia](https://forgemia.inra.fr/) en suivant la procédure proposée par sa documentation ([svn](https://forgemia.inra.fr/adminforgemia/doc-public/wikis/Migration-SVN), [git](https://forgemia.inra.fr/adminforgemia/doc-public/wikis/Migration-GIT)), cela vous convient-il?
    En l'absence de réponse d'ici 1 mois, nous feront la migration de ce projet pour des raisons d'archivage, et ce projet sera associé à un groupe dédié. 
    Bien Cordialement
    Eric Casellas pour l'équipe Record


    *Dans l'eventualité où les projets refusent ou bien s'ils sont autonomes pour assurer cette migration, la procédure s'arrête ici pour nous et c'est au projet de gérer son eventuelle migration.*

1. Pour les projets pour lesquels on prend en charge la migration du dépôt de sources, on suivra la procédure proposée par la documentation de forgemia ([svn](https://forgemia.inra.fr/adminforgemia/doc-public/wikis/Migration-SVN), [git](https://forgemia.inra.fr/adminforgemia/doc-public/wikis/Migration-GIT))
Par echange de nouveaux mail avec les responsables du projet, on obtient des informations requise pour la creation d'un projet forgemia : 
    * Nom du groupe de projets
    * Nom du projet 
    * Description du projet
    * Visibilité du projet (privé|interne|publique)
        * Pour les projets privé, la liste des membres du projet (parmis utilisateurs existant forgemia) et leur niveau de [droits](https://forgemia.inra.fr/help/user/permissions) (Guest|Reporter|Developer|Master).
    * Nom du (ou des) propriétaire(s) du projet (parmis utilisateurs existant forgemia)

1. Pour les projets actifs (ie. pouvant potentiellement avoir des nouveaux commit durant la migration), une fois que la procédure de migration est engagé, afin de limiter les risques de téléscopages entre les dépôts mulcyber et forgemia (par exemple un nouveau commit sur mulcyber alors que la migration sur forgemia à déjà été faite mais archivage mulcyber pas encore actif), on propose l'organisation suivante :
    * Prevenir N (todo: nombre à déterminer) jours à l'avance de la date butoire acceptée pour les nouveaux commits (pour laisser le temps aux developpeurs de pousser sur mulcyber leur commits et branches locales). Une fois cette date butoire atteinte on ne garantis plus que les nouveaux commits sur le dépôt mulcyber soit migrés sur le dépôt forgemia
    * On s'engage à réaliser la migration sur une plage de N (todo: nombre à déterminer) jours à partir de cette date butoire.
    * Une fois la migration effectuée et le dépôt mulcyber archivé, on informe par un dernier mail les responsables du projets (avec en particulier des informations sur la nouvelle url du dépôt forgemia, l'identifiant de la version mulcyber utilisée)
