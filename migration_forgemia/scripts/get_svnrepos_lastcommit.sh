#!/bin/bash




function get_lastcommit() {
    echo "generate statistics for repo $1"
    if [ ! -d "$1" ]; then
        echo "  cloning repo..."
        svn checkout svn+ssh://casellas@scm.mulcyber.toulouse.inra.fr/svnroot/$1/ > /dev/null || exit 1
    fi
    cd $1
    svn up > /dev/null
    svn log -r COMMITTED | sed -nE 's/^r.*\| ([0-9]{4}-[0-9]{2}-[0-9]{2} \S+ \S+).*/\1/p' 
    cd ..
}


get_lastcommit $@

<<COMMENT
./get_svnrepos_lastcommit.sh aicha
./get_svnrepos_lastcommit.sh azodyn-pois
./get_svnrepos_lastcommit.sh climatik
./get_svnrepos_lastcommit.sh florsys
./get_svnrepos_lastcommit.sh irrinovforstics
./get_svnrepos_lastcommit.sh markiz
./get_svnrepos_lastcommit.sh mfpc-lai
./get_svnrepos_lastcommit.sh micmac
./get_svnrepos_lastcommit.sh moderatosunflo
./get_svnrepos_lastcommit.sh recmoustics
./get_svnrepos_lastcommit.sh pydynamics
./get_svnrepos_lastcommit.sh recordbh
./get_svnrepos_lastcommit.sh rec-ceres
./get_svnrepos_lastcommit.sh rec_next_stics
./get_svnrepos_lastcommit.sh recordsunflo
./get_svnrepos_lastcommit.sh recordweb
./get_svnrepos_lastcommit.sh recordwebdepot
./get_svnrepos_lastcommit.sh record
./get_svnrepos_lastcommit.sh sed
./get_svnrepos_lastcommit.sh vigiemed
./get_svnrepos_lastcommit.sh rec-crash
COMMENT
