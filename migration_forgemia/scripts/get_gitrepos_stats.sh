#!/bin/bash


# Install gitstats
#sudo apt-get install gitstats

# Install gitinspector
#curl -sL https://deb.nodesource.com/setup_4.x | sudo -E bash -
#sudo apt-get install -y nodejs
#sudo npm i -g gitinspector


function get_stats() {
    echo "generate statistics for repo $1"
    echo "  cloning repo..."
    git clone git+ssh://casellas@scm.mulcyber.toulouse.inra.fr//var/lib/gforge/chroot/scmrepos/git/$1/$1.git || exit 1
    echo "  generating gitinspector html report..."
    gitinspector -HTlrm -F html $1/ >& stats_$1.html  || exit 1
    echo "  generating gitstats html report..."
    gitstats $1 stats_$1  || exit 1
}


get_stats $@


<<COMMENT
./get_gitrepos_stats.sh macsureagmip
./get_gitrepos_stats.sh agrodynaland
./get_gitrepos_stats.sh moduleindic
./get_gitrepos_stats.sh jeudoc
./get_gitrepos_stats.sh azodyn-colza
./get_gitrepos_stats.sh azodyn-orge
./get_gitrepos_stats.sh climatecafe
./get_gitrepos_stats.sh efese
./get_gitrepos_stats.sh itk-scheduler
./get_gitrepos_stats.sh phelib
./get_gitrepos_stats.sh popsyb
./get_gitrepos_stats.sh tests-stics-mod
./get_gitrepos_stats.sh recordb
./get_gitrepos_stats.sh record-daycent
./get_gitrepos_stats.sh recordschool
./get_gitrepos_stats.sh rectestcluster
./get_gitrepos_stats.sh sippom-plb
./get_gitrepos_stats.sh record-stics
./get_gitrepos_stats.sh agflow
./get_gitrepos_stats.sh vsoil
./get_gitrepos_stats.sh azodyn
./get_gitrepos_stats.sh sunflo
./get_gitrepos_stats.sh DecisionBleDur
COMMENT
