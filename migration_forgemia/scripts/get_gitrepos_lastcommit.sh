#!/bin/bash




function get_lastcommit() {
    echo "generate statistics for repo $1"
    if [ ! -d "$1" ]; then
        echo "  cloning repo..."
        git clone git+ssh://casellas@scm.mulcyber.toulouse.inra.fr//var/lib/gforge/chroot/scmrepos/git/$1/$1.git > /dev/null || exit 1
    fi
    cd $1
    git fetch --all > /dev/null 
    git log -1 --format=%ci --all
    cd ..
}


get_lastcommit $@

<<COMMENT
./get_gitrepos_lastcommit.sh macsureagmip
./get_gitrepos_lastcommit.sh agrodynaland
./get_gitrepos_lastcommit.sh moduleindic
./get_gitrepos_lastcommit.sh jeudoc
./get_gitrepos_lastcommit.sh azodyn-colza
./get_gitrepos_lastcommit.sh azodyn-orge
./get_gitrepos_lastcommit.sh climatecafe
./get_gitrepos_lastcommit.sh efese
./get_gitrepos_lastcommit.sh itk-scheduler
./get_gitrepos_lastcommit.sh phelib
./get_gitrepos_lastcommit.sh popsyb
./get_gitrepos_lastcommit.sh tests-stics-mod
./get_gitrepos_lastcommit.sh recordb
./get_gitrepos_lastcommit.sh record-daycent
./get_gitrepos_lastcommit.sh recordschool
./get_gitrepos_lastcommit.sh rectestcluster
./get_gitrepos_lastcommit.sh sippom-plb
./get_gitrepos_lastcommit.sh record-stics
./get_gitrepos_lastcommit.sh agflow
./get_gitrepos_lastcommit.sh vsoil
./get_gitrepos_lastcommit.sh azodyn
./get_gitrepos_lastcommit.sh sunflo
./get_gitrepos_lastcommit.sh DecisionBleDur
COMMENT
